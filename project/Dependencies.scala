import sbt._

object Dependencies {
  lazy val scalaTest: Seq[ModuleID] = Seq("org.scalatest" %% "scalatest" % "3.1.1" % Test)

  lazy val zioVersion = "1.0.2"
  lazy val zio: Seq[ModuleID] = Seq(
    "dev.zio"       %% "zio"                    % zioVersion,
    "dev.zio"       %% "zio-interop-cats"       % "2.1.4.0",
    "dev.zio"       %% "zio-logging"            % "0.3.2",
    "dev.zio"       %% "zio-logging-slf4j"      % "0.3.2",
    "ch.qos.logback" % "logback-classic"        % "1.2.3",
    "dev.zio"       %% "zio-metrics-prometheus" % "0.2.7",
    "dev.zio"       %% "zio-test"               % zioVersion % "test",
    "dev.zio"       %% "zio-test-sbt"           % zioVersion % "test",
    "org.typelevel" %% "kittens"                % "2.1.0"
  )

  lazy val http4s: Seq[ModuleID] = Seq(
    "org.http4s" %% "http4s-dsl",
    "org.http4s" %% "http4s-blaze-server",
    "org.http4s" %% "http4s-blaze-client",
    "org.http4s" %% "http4s-circe"
  ).map(_ % "0.21.6")

  lazy val circe: Seq[ModuleID] =
    Seq("io.circe" %% "circe-core", "io.circe" %% "circe-generic", "io.circe" %% "circe-parser", "io.circe" %% "circe-refined")
      .map(_ % "0.13.0")

  lazy val pureConfig = Seq("com.github.pureconfig" %% "pureconfig" % "0.13.0")

  private val tsecVersion = "0.2.1"
  lazy val tsec: Seq[ModuleID] = Seq(
    "io.github.jmcardon" %% "tsec-common",
    "io.github.jmcardon" %% "tsec-password",
    "io.github.jmcardon" %% "tsec-cipher-jca",
    "io.github.jmcardon" %% "tsec-cipher-bouncy",
    "io.github.jmcardon" %% "tsec-mac",
    "io.github.jmcardon" %% "tsec-signatures",
    "io.github.jmcardon" %% "tsec-hash-jca",
    "io.github.jmcardon" %% "tsec-hash-bouncy",
    //"io.github.jmcardon" %% "tsec-libsodium",
    "io.github.jmcardon" %% "tsec-jwt-mac",
    "io.github.jmcardon" %% "tsec-jwt-sig",
    "io.github.jmcardon" %% "tsec-http4s"
  ).map(_ % tsecVersion)

  lazy val skunk = Seq("org.tpolecat" %% "skunk-core" % "0.0.21")

  object Tapir {
    private val version = "0.16.5"

    lazy val core             = "com.softwaremill.sttp.tapir"  %% "tapir-core"               % version
    lazy val docs             = "com.softwaremill.sttp.tapir"  %% "tapir-openapi-docs"       % version
    lazy val openApiCirce     = "com.softwaremill.sttp.tapir"  %% "tapir-openapi-circe"      % version
    lazy val openApiCirceYaml = "com.softwaremill.sttp.tapir"  %% "tapir-openapi-circe-yaml" % version
    lazy val sttp             = "com.softwaremill.sttp.tapir"  %% "tapir-sttp-client"        % version
    lazy val http4s           = "com.softwaremill.sttp.tapir"  %% "tapir-http4s-server"      % version
    lazy val zio              = "com.softwaremill.sttp.tapir"  %% "tapir-zio"                % version
    lazy val zioHttp4sServer  = "com.softwaremill.sttp.tapir"  %% "tapir-zio-http4s-server"  % version
    lazy val circe            = "com.softwaremill.sttp.tapir"  %% "tapir-json-circe"         % version
    lazy val swagger          = "com.softwaremill.sttp.tapir"  %% "tapir-swagger-ui-http4s"  % version
    lazy val http4sClient     = "com.softwaremill.sttp.client" %% "http4s-backend"           % "2.2.1"
    lazy val refined          = "com.softwaremill.sttp.tapir"  %% "tapir-refined"            % version
  }

  lazy val tapir: Seq[ModuleID] = Seq(
    Tapir.core,
    Tapir.docs,
    Tapir.openApiCirce,
    Tapir.openApiCirceYaml,
    Tapir.sttp,
    Tapir.http4s,
    Tapir.zio,
    Tapir.zioHttp4sServer,
    Tapir.circe,
    Tapir.swagger,
    Tapir.http4sClient,
    Tapir.refined
  )

  lazy val refined =
    Seq(
      "eu.timepit" %% "refined"      % "0.9.15",
      "eu.timepit" %% "refined-cats" % "0.9.15"
    )

  lazy val facebookSdk = Seq("com.facebook.business.sdk" % "facebook-java-business-sdk" % "9.0.0")

  lazy val newtype = Seq("io.estatico" %% "newtype" % "0.4.4")

  lazy val flyway = Seq("org.flywaydb" % "flyway-core" % "7.3.2", "org.postgresql" % "postgresql" % "42.2.14")

  lazy val zioGrpc =
    Seq(
      "io.grpc"               % "grpc-netty"           % "1.31.1",
      "com.thesamet.scalapb" %% "scalapb-runtime-grpc" % scalapb.compiler.Version.scalapbVersion,
      "com.thesamet.scalapb" %% "scalapb-json4s"       % "0.10.1"
    )

  lazy val guava = Seq("com.google.guava" % "guava" % "20.0")

  lazy val kamon = Seq(
    "io.kamon" %% "kamon-bundle"         % "2.1.4",
    "io.kamon" %% "kamon-core"           % "2.1.0",
    "io.kamon" %% "kamon-status-page"    % "2.1.4",
    "io.kamon" %% "kamon-system-metrics" % "2.1.0",
    "io.kamon" %% "kamon-prometheus"     % "2.1.0",
    "io.kamon" %% "kamon-http4s"         % "2.0.1",
    "io.kamon" %% "kamon-zipkin"         % "2.1.0"
  )

  lazy val stripe = Seq(
    "com.stripe" % "stripe-java" % "20.24.0"
  )

  lazy val quicklens = Seq(
    "com.softwaremill.quicklens" %% "quicklens" % "1.6.1"
  )

  lazy val curator = Seq(
    "org.apache.curator" % "curator-recipes" % "5.1.0"
  )

}
