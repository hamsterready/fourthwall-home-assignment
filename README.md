# Fourthwall Home Assignment

The details of the home assignment can be found
[here](https://gist.github.com/wbaumann/aaa5ef095e213ffbea35b7ca3cc251a7) and in
the [docs](./docs/BackendCodingChallenge_README.md) directory.


# Choices

Solution proposed here is built with Scala, HTTP4s, Tapir and ZIO. 

HTTP4s/Tapir are being used to handle OpenAPI aspect of the task.

Scala/ZIO - author of the solution has not been in the Spring/Java world for
sometime now so decided to go with his currently preferred stack.

Persistence - we will use PostgreSQL and
[non-JDBC-compliant](https://tpolecat.github.io/skunk/index.html) access library
from [Rob Norris (tpolecat)](https://github.com/tpolecat) - see this
[talk](https://www.youtube.com/watch?v=NJrgj1vQeAI&ab_channel=ScalaDaysConferences)
for details.

Authorization, authentication and access control are the obvious element that we
will leave unimplemented. Based on the previous experience author would advocate
for sticking to the standards (like OAuth).

# Running

Local dev environment via docker.

```
cd dev-env && docker-compose up -d
```

Point your browser to: http://localhost:8080/TODO

# Development

Use `sbt` and your favorite IDE.




