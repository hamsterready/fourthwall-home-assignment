package cinema.log

import zio.console.Console
import zio.logging.slf4j.Slf4jLogger
import zio.logging.{LogContext, Logging => ZLogging}
import zio.{UIO, ZIO, ZLayer}

object Logging {

  def live: ZLayer[Any, Nothing, ZLogging] =
    Slf4jLogger.make((_, logEntry) => logEntry)

  def test(enabled: Boolean): ZLayer[Console, Nothing, ZLogging] =
    (for {
      console <- ZIO.service[Console.Service]
    } yield new zio.logging.Logger[String] {
      override def locally[R1, E, A1](
          f: LogContext => LogContext
      )(zio: ZIO[R1, E, A1]): ZIO[R1, E, A1] = zio

      override def log(line: => String): UIO[Unit] =
        console.putStrLn(line).when(enabled)

      override def logContext: UIO[LogContext] =
        ZIO.succeed(LogContext(Map.empty))
    }).toLayer
}
