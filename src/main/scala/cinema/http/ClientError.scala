package cinema.http

import io.circe.Codec
import io.circe.generic.semiauto.deriveCodec
import sttp.model.StatusCode
import sttp.tapir.EndpointOutput
import sttp.tapir.json.circe.jsonBody
import sttp.tapir.ztapir.{statusMapping, _}
import zio._
import zio.logging.{Logging, log}

sealed trait ClientError extends Product with Serializable

object ClientError {

  final case class RefineException(message: String) extends Exception

  final case class Unauthorized(error: String) extends ClientError
  final object Unauthorized {
    implicit val codec: Codec[Unauthorized] = deriveCodec
  }
  final case class InternalServerError(error: String) extends ClientError
  object InternalServerError {
    implicit val codec: Codec[InternalServerError] = deriveCodec
  }

  final case class BadRequest(error: String) extends ClientError
  object BadRequest {
    implicit val codec: Codec[BadRequest] = deriveCodec
  }

  final case class NotFound(error: String) extends ClientError
  object NotFound {
    implicit val codec: Codec[NotFound] = deriveCodec
  }

  /**
    * Expose the full error cause when handling a request and recovers failures and deaths appropriately.
    */
  implicit class mapError[R, E, A](private val f: ZIO[R, E, A]) extends AnyVal {
    def mapToClientError: ZIO[R with Logging, ClientError, A] =
      f.tapCause(error =>
          log.error(s"Error processing request ${error.prettyPrint}")
        )
        .mapErrorCause { cause =>
          val clientFailure = cause.failureOrCause match {
            case _ => InternalServerError("Internal Server Error")
          }
          Cause.fail(clientFailure)
        }
  }

  val httpErrors: EndpointOutput.OneOf[ClientError, ClientError] = oneOf(
    statusMapping(
      StatusCode.InternalServerError,
      jsonBody[InternalServerError]
    ),
    statusMapping(StatusCode.BadRequest, jsonBody[BadRequest]),
    statusMapping(StatusCode.NotFound, jsonBody[NotFound]),
    statusMapping(StatusCode.Unauthorized, jsonBody[Unauthorized])
  )

}
