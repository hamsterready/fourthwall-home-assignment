package cinema.http.routes

import cats.kernel.Semigroup
import org.http4s.HttpRoutes
import sttp.tapir.openapi.OpenAPI
import sttp.tapir.openapi.circe.yaml._
import sttp.tapir.swagger.http4s.SwaggerHttp4s
import zio.Task
import zio.interop.catz._

object OpenApi {
  implicit val s: Semigroup[OpenAPI] = new Semigroup[OpenAPI] {
    override def combine(x: OpenAPI, y: OpenAPI): OpenAPI =
      y.paths.foldLeft(x) { case (t, (p, i)) => t.addPathItem(p, i) }
  }

  /**
    * Exposes swagger routes
    */
  def routes: HttpRoutes[Task] =
    new SwaggerHttp4s(MoviesApi.docs.toYaml)
      .routes[Task]
}
