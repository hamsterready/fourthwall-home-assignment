package cinema

import cinema.config.Configuration
import cinema.config.Configuration.Configuration
import cinema.db.Persistence.Persistence
import cinema.db.{DatabaseSessionPool, Migrations, Persistence}
import cinema.log.Logging
import cinema.http.HttpServer
import zio._
import zio.blocking.Blocking
import zio.clock.Clock
import zio.console.Console
import zio.logging.{Logging => ZLogging}

object MyApp extends App {
  type AppEnvironment =
    Console
      with Configuration
      with Clock
      with Blocking
      with Persistence
      with ZLogging

  private val persistenceLive =
    (Configuration.live >>> DatabaseSessionPool.live) >>> Persistence.live

  private val env: ZLayer[Any, Throwable, AppEnvironment] =
    Console.live ++
      Blocking.live ++
      Clock.live ++
      Configuration.live ++
      Logging.live ++
      persistenceLive

  def run(args: List[String]): URIO[ZEnv, ExitCode] = {
    val program = for {
      serverFiber <- Migrations.migrate *> HttpServer.runServer
      _ <- serverFiber.join
    } yield ()
    program.provideLayer(env).exitCode
  }
}
