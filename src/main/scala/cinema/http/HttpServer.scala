package cinema.http

import cats.implicits._
import cinema.MyApp.AppEnvironment
import cinema.config.Configuration
import cinema.http.routes._
import org.http4s.HttpRoutes
import org.http4s.implicits._
import org.http4s.server.blaze.BlazeServerBuilder
import zio.clock.Clock
import zio.interop.catz._
import zio.interop.catz.implicits._
import zio.{Task, _}

object HttpServer {

  private def routes(): ZIO[AppEnvironment, Nothing, HttpRoutes[Task]] =
    for {
      movies <- MoviesApi.routes
      docs = OpenApi.routes
    } yield docs <+> movies

  def runServer: URIO[AppEnvironment, Fiber.Runtime[Throwable, Unit]] =
    ZIO
      .runtime[Clock]
      .flatMap { implicit rts =>
        for {
          config <- Configuration.load
          app <- routes()
          bec <- blocking.blocking(ZIO.descriptor.map(_.executor.asEC))
          _ <-
            BlazeServerBuilder[Task](bec)
              .bindHttp(config.httpServer.port, config.httpServer.host)
              .withoutBanner
              .withNio2(true)
              .withHttpApp(app.orNotFound)
              .serve
              .compile
              .drain
        } yield ()
      }
      .fork
}
