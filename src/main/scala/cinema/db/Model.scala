package cinema.db

import cats.Show
import eu.timepit.refined.types.string.NonEmptyString

object types {

  implicit val n: Show[NonEmptyString] = Show(x => x.value)

}
