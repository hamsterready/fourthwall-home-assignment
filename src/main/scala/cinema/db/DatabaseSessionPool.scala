package cinema.db

import cinema.config.Configuration
import cinema.config.Configuration.Configuration
import natchez.Trace.Implicits.noop
import skunk.Session
import zio._
import zio.interop.catz._

object DatabaseSessionPool {

  type DatabaseSessionPool = Has[TaskManaged[Session[Task]]]

  private val managedSessionPool: ZManaged[Configuration, Throwable, TaskManaged[Session[Task]]] = {
    val s = for {
      config <- Configuration.load
      session <-
        ZIO
          .runtime[Any]
          .map { implicit rts =>
            Session
              .pooled[Task](
                host = config.database.host,
                port = config.database.port,
                user = config.database.user,
                database = config.database.database,
                password = config.database.password,
                max = 20,
                debug = false
              )
              .toManagedZIO
          }
    } yield session

    s.toManaged_.flatten.map(_.toManagedZIO)
  }

  def live: ZLayer[Configuration, Throwable, DatabaseSessionPool] =
    ZLayer.fromManaged(managedSessionPool)

  /**
    * Exposes the pool of [[Session]]
    */
  def pool: URIO[DatabaseSessionPool, TaskManaged[Session[Task]]] = ZIO.service[TaskManaged[Session[Task]]]
}
