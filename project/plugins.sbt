addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.7.4")

addSbtPlugin("ch.epfl.scala" % "sbt-scalafix" % "0.9.21")

addSbtPlugin("org.wartremover" % "sbt-wartremover" % "2.4.10")

addSbtPlugin("com.thesamet" % "sbt-protoc" % "0.99.34")

addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.10.0-RC1")

addSbtPlugin("org.jmotor.sbt" % "sbt-dependency-updates" % "1.2.2")

libraryDependencies +=
  "com.thesamet.scalapb.zio-grpc" %% "zio-grpc-codegen" % "0.4.0"

addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.7.5")

addSbtPlugin("com.lightbend.sbt" % "sbt-javaagent" % "0.1.5")
