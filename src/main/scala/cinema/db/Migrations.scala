package cinema.db

import cinema.config.Configuration
import cinema.config.Configuration.{Configuration, DatabaseConfig}
import org.flywaydb.core.Flyway
import org.flywaydb.core.api.output.MigrateResult
import zio.blocking._
import zio.{Fiber, RIO, ZIO}

object Migrations {

  private def fly(config: DatabaseConfig): RIO[Blocking, MigrateResult] =
    effectBlocking(Flyway.configure().dataSource(config.url, config.user, config.password.orNull).load().migrate())

  def migrate: ZIO[Blocking with Configuration, Throwable, Fiber.Runtime[Throwable, MigrateResult]] =
    for {
      config <- Configuration.load
      task   <- fly(config.database).fork
    } yield task
}
