package cinema.config

import cats.implicits._
import pureconfig._
import pureconfig.generic.auto._
import zio.{Has, Task, ZIO, ZLayer}

object Configuration {

  final case class HttpServerConfig(port: Int, host: String)

  final case class DatabaseConfig(
      host: String,
      port: Int,
      user: String,
      database: String,
      password: Option[String]
  ) {
    def url: String = show"jdbc:postgresql://$host:$port/$database"
  }

  /**
    * Top-level application configuration.
    *
    * @param httpServer - HTTP server configuration
    * @param database - Database access details configuration
    */
  final case class AppConfig(
      httpServer: HttpServerConfig,
      database: DatabaseConfig
  )

  trait Service {
    def load: Task[AppConfig]
  }

  type Configuration = Has[Service]

  def load: ZIO[Configuration, Throwable, AppConfig] =
    ZIO.accessM[Configuration](_.get.load)

  def live: ZLayer[Any, Throwable, Configuration] =
    ZLayer.succeed(new Service {
      override def load: Task[AppConfig] =
        Task(ConfigSource.default.loadOrThrow[AppConfig])
    })
}
