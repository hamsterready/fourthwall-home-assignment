package cinema.http.routes

import cinema.config.Configuration
import cinema.config.Configuration.Configuration
import cinema.http.ClientError
import cinema.http.ClientError._
import org.http4s.HttpRoutes
import sttp.tapir.Endpoint
import sttp.tapir.docs.openapi._
import sttp.tapir.openapi.OpenAPI
import sttp.tapir.server.http4s.ztapir._
import sttp.tapir.ztapir.{endpoint, _}
import zio._
import zio.interop.catz._
import zio.logging.Logging

object MoviesApi {

  private def webhookHandler: Endpoint[String, ClientError, Unit, Nothing] =
    endpoint.post
      .in("api/v1/xxx")
      .in(stringBody)
      .errorOut(httpErrors)

  private def webhookHandlerRoute
      : URIO[Configuration with Logging, HttpRoutes[Task]] =
    webhookHandler.toRoutesR { _ =>
      val response = for {
        _ <- Configuration.load
      } yield ()
      response.mapToClientError
    }

  def routes: ZIO[Configuration with Logging, Nothing, HttpRoutes[Task]] =
    for {
      webhookHandler <- webhookHandlerRoute
    } yield webhookHandler

  val docs: OpenAPI = List(webhookHandler).toOpenAPI("Movies API", "0.1")

}
