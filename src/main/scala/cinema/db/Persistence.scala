package cinema.db

import cinema.db.DatabaseSessionPool.DatabaseSessionPool
import skunk.Session
import zio._
import zio.interop.catz._

object Persistence {

  trait Service {}

  type Persistence = Has[Service]

  def live: ZLayer[DatabaseSessionPool, Throwable, Persistence] =
    (for {
      pool <- DatabaseSessionPool.pool
    } yield new Service with Queries {

      private def inTransaction[E, A](body: => RIO[Session[Task], A]): Task[A] =
        pool.use(s => s.transaction.use(_ => body.provide(s)))

      def hello(): Task[Nothing] = inTransaction(???)

    }).toLayer

  def test(): ZLayer[Any, Throwable, Persistence] =
    ZLayer.succeed(
      new Service {}
    )

}
